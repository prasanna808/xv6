var searchData=
[
  ['backcmd_973',['backcmd',['../dd/da3/sh_8c.html#a8f1212e0b392bac41dcc6ff160b5dcdb',1,'sh.c']]],
  ['balloc_974',['balloc',['../d7/d1f/mkfs_8c.html#a327cdfc7a74165d8922ec6c8ba256906',1,'mkfs.c']]],
  ['begin_5fop_975',['begin_op',['../d5/d64/defs_8h.html#a603ca98212e00d2ffdba7827ef0f1003',1,'begin_op():&#160;log.c'],['../d7/df8/log_8c.html#ac96aa31ffc0500e749c62c4d377c21c9',1,'begin_op(void):&#160;log.c']]],
  ['bigargtest_976',['bigargtest',['../de/dc0/usertests_8c.html#ac5b5d4517586be6396b4f3b37445945a',1,'usertests.c']]],
  ['bigdir_977',['bigdir',['../de/dc0/usertests_8c.html#aa0d46fcb43667c805b84ea7e1ff6100a',1,'usertests.c']]],
  ['bigfile_978',['bigfile',['../de/dc0/usertests_8c.html#a86fe524d99d69c18a335512cf3405b31',1,'usertests.c']]],
  ['bigwrite_979',['bigwrite',['../de/dc0/usertests_8c.html#a57e55b2d0161d62bb99765ca3de43e35',1,'usertests.c']]],
  ['binit_980',['binit',['../dc/de6/bio_8c.html#a53cca0ddc98c5f1de37124eca2575a59',1,'binit(void):&#160;bio.c'],['../d5/d64/defs_8h.html#a53cca0ddc98c5f1de37124eca2575a59',1,'binit(void):&#160;bio.c']]],
  ['bootmain_981',['bootmain',['../d5/dfc/bootmain_8c.html#a0d198d492591e1b70a8a12109408a7e4',1,'bootmain.c']]],
  ['bread_982',['bread',['../dc/de6/bio_8c.html#ae000984516278965dde3d125affd086c',1,'bread(uint dev, uint blockno):&#160;bio.c'],['../d5/d64/defs_8h.html#a90c72cf2140fdf1612484117327220af',1,'bread(uint, uint):&#160;bio.c']]],
  ['brelse_983',['brelse',['../dc/de6/bio_8c.html#ab5335aeb503731104314321a78a6d727',1,'brelse(struct buf *b):&#160;bio.c'],['../d5/d64/defs_8h.html#aa31ec2f79e0456737a9680270bc1841b',1,'brelse(struct buf *):&#160;bio.c']]],
  ['bsstest_984',['bsstest',['../de/dc0/usertests_8c.html#a0b0ff95e564df3c3e8fc977226b75de7',1,'usertests.c']]],
  ['bwrite_985',['bwrite',['../dc/de6/bio_8c.html#a63c899c13b176ddf80064d32225e1298',1,'bwrite(struct buf *b):&#160;bio.c'],['../d5/d64/defs_8h.html#a1bfd775f14ad3dfee354ee3897ecd28d',1,'bwrite(struct buf *):&#160;bio.c']]]
];
