var searchData=
[
  ['g_220',['g',['../d8/d3e/structsegdesc.html#a6af7593606fa1a1ff003b4a78facc8e0',1,'segdesc']]],
  ['gatedesc_221',['gatedesc',['../d8/dd3/structgatedesc.html',1,'']]],
  ['gdt_222',['gdt',['../db/d62/structcpu.html#aee38fb8832f8e728538b2cee877d1c09',1,'cpu']]],
  ['getcallerpcs_223',['getcallerpcs',['../d5/d64/defs_8h.html#a4105de9e2969515d6c6c795c4386f69f',1,'getcallerpcs(void *, uint *):&#160;defs.h'],['../d3/d2d/spinlock_8c.html#a6ac35304ea80f01086b47edcc2328010',1,'getcallerpcs(void *v, uint pcs[]):&#160;spinlock.c']]],
  ['getcmd_224',['getcmd',['../dd/da3/sh_8c.html#a0ee5a9418fea2f92036f5367a4101fe8',1,'sh.c']]],
  ['getpid_225',['getpid',['../d8/ddb/user_8h.html#a939cb25a305fe68aad9b365077f1a8c7',1,'user.h']]],
  ['gets_226',['gets',['../db/d8a/ulib_8c.html#a3702e71383a007c873d8e7b84dcbe62a',1,'gets(char *buf, int max):&#160;ulib.c'],['../d8/ddb/user_8h.html#abab901a6ac270dc88187de0ea891a202',1,'gets(char *, int max):&#160;ulib.c']]],
  ['gettoken_227',['gettoken',['../dd/da3/sh_8c.html#aaf1dedac6db801eab8d23f2acb3b1272',1,'sh.c']]],
  ['grep_2ed_228',['grep.d',['../df/d5e/grep_8d.html',1,'']]],
  ['growproc_229',['growproc',['../d5/d64/defs_8h.html#acb02e9289fb8a1017c3455b137a9bccd',1,'growproc(int):&#160;proc.c'],['../d3/dda/proc_8c.html#a9c16214741f4fcd088e5eea468709328',1,'growproc(int n):&#160;proc.c']]],
  ['gs_230',['gs',['../dc/dd1/structtaskstate.html#a15529ac51461a168be78130042a740e7',1,'taskstate::gs()'],['../df/d05/structtrapframe.html#a3df62e3ed00405cbaaa4d72902c617da',1,'trapframe::gs()']]]
];
