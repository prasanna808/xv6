#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

void first_question(char *filename, int offset, int length, char *string) {
	int fd;
	//int ret;
	char buff[length + 1];
	int str_len = 0;
	
	fd = open(filename, O_RDONLY);

	if(fd < 0) {
		//perror("open");
		exit();
	};
	
	while(string[str_len] != '\0') {
		str_len++;
	
	}

	lseek(fd, offset, SEEK_CUR);
	
	if(fd < 0) {
		//perror("lseek");
		close(fd);
		exit();
	}

	read(fd, buff, length);

	if(fd < 0) {
		//perror("read");
		close(fd);
		exit();
	}

	if(str_len == length) {
		int i;
		int count = 0;
		for(i = 0; i < length; i++) {
			if(string[i] == string[i]) {
				count++;
			}
		}
		if(count == length) {
			printf(1,"same string verified\n");
		}
		else {
			printf(1,"different string \n");
		}
	}
	else {
		printf(1,"string rejected");
	}

	close(fd);
}

int
main(int argc, char *argv[])
{
  	char *str = "prasanna";
	first_question(argv[1], 20, 8, str);	
  	exit();
}
